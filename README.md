[Version]:1
[Grid]:grid.kitely.com:8002:The%20Collective
[Comment]:Added%20a%20Comment%20Function!
[Url]:https://bitbucket.org/thecollectivesl/tc-simlink
[end]:end
![SimLink](https://bytebucket.org/thecollectivesl/tc-simlink/raw/master/object/SimLink.png)
# The Collective SimLink Chat #

This is a script that allows for messages that are sent in chat to be relayed to other sims / grids.

### How it works ###

This chat link works by creating a uuid from [here](http://gridurl.appspot.com/random) and putting it in the description of the prim that the script is put into. 
Thats it! It will link to the other object automatically and turn green when it is ready to be used.

Please note this is currently designed for a 1-1 prim link. You will need to use more than 1 set of these to be able to link to multiple sims / grids. Feel free to fork this if you would like to add to it!

### Features ###

* Extend chat to other grids / sims
* Can work with grids that go offline (Kitely or intermitent simulations)

### Who do I talk to? ###

* [Kurtis Anatine](https://www.kitely.com/forums/memberlist.php?mode=viewprofile&u=4102)
* [Shandon Loring](https://www.kitely.com/forums/memberlist.php?mode=viewprofile&u=1178)


Copyright Kurtis Anatine, 2017, LGPL, http://www.gnu.org/copyleft/lesser.html