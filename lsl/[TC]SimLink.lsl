/*
	[TC]SimLink.ossl - Made by Kurtis Anatine

	Notes: A simple update checker for markdowns if you host code on bitbucket / github
	Copyright Kurtis Anatine, 2017, LGPL, http://www.gnu.org/copyleft/lesser.html
*/
// Global Lists
// Update Checker List
list lst_update	=	[																						// Update Variables
						1,																					// 0 Version Number! Put your current one here! (i would recommend using something like unix time or the date in a integer)
						NULL_KEY,																			// 1 Http Request Key
						"https://bitbucket.org/thecollectivesl/tc-simlink/raw/master/README.md"		// 2 URL to markdown
					];
list lst_html	 =	[			// This is all the html calls
					NULL_KEY,	// 0 UUID For Chat Link, Get this id from  
					NULL_KEY,	// 1 HTTP Event Holder
					"",			// 2 Local HTTP Address
					NULL_KEY,	// 3 HTTP Event Holder
					"",			// 4 Remote HTTP Address
					NULL_KEY,	// 5 HTTP Event Handler
					NULL_KEY,	// 6 HTTP Event Handler
					NULL_KEY	// 7 HTTP Event Handler
					];
list lst_listen	=	[			// Listen calls
					0,			// 0 Listen Channel
					0			// 1 Listen Event Holder
					];
list lst_chat;
// Global Variables
// Global Functions
UPDATECHK()
{
	lst_update = llListReplaceList(lst_update, [llHTTPRequest(llList2String(lst_update, 2), [], "")], 1, 1);
}
INIT()	  
{
	llSetTimerEvent(0);
	UPDATECHK();
	llListenRemove(llList2Integer(lst_listen, 1));
	lst_listen = llListReplaceList(lst_listen, [llListen(llList2Integer(lst_listen, 0), "", "", "")], 1, 1);
	llReleaseURL(llList2String(lst_html, 2));
	lst_html = llListReplaceList(lst_html, [""], 2, 2);
	lst_html = llListReplaceList(lst_html, [""], 4, 4);
	if(llGetAttached())
	{
		llRequestPermissions(llGetOwner(), PERMISSION_TAKE_CONTROLS);
	}
	if(llGetObjectDesc() != "")
	{
		if((key)llGetObjectDesc())
		{
			lst_html = llListReplaceList(lst_html, [llGetObjectDesc()], 0, 0);
			llRequestURL();
		}
		else
		{
			llSay(0,"The UUID in my description does not apper to be valid :/. Please goto http://gridurl.appspot.com/random and get a uuid and put this into my description. Or use one you already have to link me to another SimLink! I will Check again shortly");
			llSetTimerEvent(10);
		}
	}
	else
	{
		llSay(0,"Please goto http://gridurl.appspot.com/random and get a uuid and put this into my description. Or use one you already have to link me to another SimLink! I will Check again shortly");
		llSetTimerEvent(10);
	}
}
// Main Script
default
{
	changed(integer change)
	{
		if(change & CHANGED_REGION)
		{
			INIT();
		}
	}
	on_rez(integer start_param)
	{
		INIT();
	}
	run_time_permissions(integer perm)
	{
		if(perm & PERMISSION_TAKE_CONTROLS)
		{
			llTakeControls(0, FALSE, TRUE);
		}
	}
	state_entry()
	{
		INIT();
	}
	http_request(key id, string method, string body)
	{
		if(method == URL_REQUEST_GRANTED)
		{
			llSetColor(<1,1,0>, ALL_SIDES);
			if(llGetSubString(body, -1, -1) != "/")
			{
				body += "/";
			}
			lst_html = llListReplaceList(lst_html, [body], 2, 2);
			lst_html = llListReplaceList(lst_html, [llHTTPRequest("http://gridurl.appspot.com/get/"+llList2String(lst_html, 0), [HTTP_METHOD, "GET"], "")], 1, 1);
		}
		else if(method == URL_REQUEST_DENIED)
		{
			llSetColor(<1,0,0>, ALL_SIDES);
			lst_html = llListReplaceList(lst_html, [""], 2, 2);
			llSay(0,"Could not get a URL :/ going to try again in a minute.");
			llSetTimerEvent(60);
		}
		else if(method == "GET")
		{
			llSetColor(<1,1,0>, ALL_SIDES);
			llHTTPResponse(id, 200, "TEST|OK");
		}
		else if(method == "POST")
		{
			llSetColor(<1,1,0>, ALL_SIDES);
			list lst_data = llParseString2List(body, ["|"], []);
			if(llList2String(lst_data, 0) == "CON")
			{
				lst_html = llListReplaceList(lst_html, [llUnescapeURL(llList2String(lst_data, 1))], 4, 4);
				llSetTimerEvent(1);
				llHTTPResponse(id, 200, "CON|OK");
			}
			else if(llList2String(lst_data, 0) == "CHAT")
			{
				lst_data = llDeleteSubList(lst_data, 0, 0);
				while(llGetListLength(lst_data) != 0)
				{
					llSay(0,llList2String(lst_data, 0)+": "+llList2String(lst_data, 1));
					lst_data = llDeleteSubList(lst_data, 0, 1);
				}
				llHTTPResponse(id, 200, "CHAT|OK");
			}
		}
	}
	http_response(key request_id, integer status, list metadata, string body)
	{
		if(request_id == llList2Key(lst_update, 1))
		{
			integer int_updateindex = llSubStringIndex(body, "[end]:end");
			if(int_updateindex != -1)
			{
				list lst_data = llParseString2List(llUnescapeURL(llGetSubString(body, 0, int_updateindex - 1)), ["]:","\n"], []);
				if(llList2Integer(lst_data, 1) > llList2Integer(lst_update, 0))
				{
					llOwnerSay(llGetScriptName()+": There is an update for me!\nGrab it at "+llList2String(lst_data, 3)+"\nChanges: "+llList2String(lst_data, 5)+"\nYou can get the latest at "+llList2String(lst_data, 7));
				}
				else
				{
					llOwnerSay(llGetScriptName()+" Is up to date!");
				}
			}
		}
		if(llList2Key(lst_html, 1) == request_id)
		{
			if(status == 200)
			{
				if(body != llList2String(lst_html, 2))
				{// if we are the main node then we need to wait for the other one to connect to us 8)
					if(llSubStringIndex(body, "ERROR") == -1)
					{
						lst_html = llListReplaceList(lst_html, [body], 4, 4);
						lst_html = llListReplaceList(lst_html, [llHTTPRequest(llList2String(lst_html, 4), [HTTP_METHOD, "POST"], "CON|"+llEscapeURL(llList2String(lst_html, 2)))], 3, 3);
					}
					else
					{// this is a new uuid that hasnt been used so set our url as the start one.
						lst_html = llListReplaceList(lst_html, [llHTTPRequest("http://gridurl.appspot.com/reg?service="+llList2String(lst_html, 0)+"&url="+llEscapeURL(llList2String(lst_html, 2)), [HTTP_METHOD, "GET"], "")], 5, 5);
					}
				}
				else
				{// just have to wait till the other sides ready
					llSetTimerEvent(60);
				}
			}
			else
			{
				if(llSubStringIndex(body, "ERROR") != -1)
				{
					lst_html = llListReplaceList(lst_html, [llHTTPRequest("http://gridurl.appspot.com/reg?service="+llList2String(lst_html, 0)+"&url="+llEscapeURL(llList2String(lst_html, 2)), [HTTP_METHOD, "GET"], "")], 5, 5);
				}
				else
				{
					llSetColor(<1,1,0>, ALL_SIDES);
					lst_html = llListReplaceList(lst_html, [""], 4, 4);
					llSetTimerEvent(60);
					llSay(0,"Problem getting remote SimLink, will try again in a minute!");
				}
			}
		}
		else if(llList2Key(lst_html, 3) == request_id)
		{
			if(status == 200)
			{
				list lst_data = llParseString2List(body, ["|"], []);
				if(llList2String(lst_data, 0) == "CON")
				{
					if(llList2String(lst_data, 1) == "OK")
					{
						llSetColor(<0,1,0>, ALL_SIDES);
						lst_chat = [];
						llSetTimerEvent(1);
						llSay(0,"Connected!");
					}
				}
				else
				{ // we got something that wasnt a connection back from the other side, replace the url with our own and wait for someone to hit it.
					lst_html = llListReplaceList(lst_html, [llHTTPRequest("http://gridurl.appspot.com/reg?service="+llList2String(lst_html, 0)+"&url="+llEscapeURL(llList2String(lst_html, 2)), [HTTP_METHOD, "GET"], "")], 5, 5);
				}
			}
			else
			{// does not exist anymore or we got a 404 so we need to update the grid url with ours
				lst_html = llListReplaceList(lst_html, [llHTTPRequest("http://gridurl.appspot.com/reg?service="+llList2String(lst_html, 0)+"&url="+llEscapeURL(llList2String(lst_html, 2)), [HTTP_METHOD, "GET"], "")], 5, 5);
			}
		}
		else if(llList2Key(lst_html, 5) == request_id)
		{// Updating the html 
			if(status != 200 && body != "OK")
			{
				llSetColor(<1,0,0>, ALL_SIDES);
				lst_html = llListReplaceList(lst_html, [""], 4, 4);
				llSetTimerEvent(60);
				llSay(0,"Could not update the remote connector! Will try again in about a minute");
			}
		}
		else if(llList2Key(lst_html, 6) == request_id)
		{
			if(status == 200)
			{
				llSetColor(<0,1,0>, ALL_SIDES);
			}
			else
			{
				llSetColor(<1,0,0>, ALL_SIDES);
				llSay(0,"Connection lost! Trying to reconnect");
				lst_html = llListReplaceList(lst_html, [""], 4, 4);
				lst_html = llListReplaceList(lst_html, [llHTTPRequest("http://gridurl.appspot.com/get/"+llList2String(lst_html, 0), [HTTP_METHOD, "GET"], "")], 1, 1);
			}
		}
		else if(llList2Key(lst_html, 7) == request_id)
		{
			if(status != 200 && body != "TEST|OK")
			{// do the whole url grab thing again
				llSetColor(<1,0,0>, ALL_SIDES);
				llReleaseURL(llList2String(lst_html, 2));
				lst_html = llListReplaceList(lst_html, [""], 2, 2);
				lst_html = llListReplaceList(lst_html, [""], 4, 4);
				llRequestURL();
			}
		}
	}
	timer()
	{
		if(llGetTime() > 600)
		{
			llResetTime();
			lst_html = llListReplaceList(lst_html, [llHTTPRequest(llList2String(lst_html, 4), [HTTP_METHOD, "GET"], "")], 6, 6);
		}
		if(llList2Key(lst_html, 0) == NULL_KEY)
		{
			if(llGetObjectDesc() != "")
			{
				if((key)llGetObjectDesc())
				{
					lst_html = llListReplaceList(lst_html, [llGetObjectDesc()], 0, 0);
					llRequestURL();
				}
			}
		}
		else if(llList2String(lst_html, 2) == "")
		{
			llRequestURL();
		}
		else if(llList2String(lst_html, 4) == "")
		{
			lst_html = llListReplaceList(lst_html, [llHTTPRequest("http://gridurl.appspot.com/get/"+llList2String(lst_html, 0), [HTTP_METHOD, "GET"], "")], 1, 1);
		}
		else if(llGetListLength(lst_chat))
		{
			lst_html = llListReplaceList(lst_html, [llHTTPRequest(llList2String(lst_html, 4), [HTTP_METHOD, "POST", HTTP_BODY_MAXLENGTH, 16384], "CHAT|"+llDumpList2String(lst_chat, "|"))], 6, 6);
			lst_chat = [];
		}
	}
	listen(integer channel, string name, key id, string message)
	{
		if(llGetAgentSize(id) != ZERO_VECTOR && llList2String(lst_html , 4) != "")
		{
			lst_chat += llKey2Name(id);
			lst_chat += message;
		}	
	}
}